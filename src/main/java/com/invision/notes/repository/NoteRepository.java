package com.invision.notes.repository;

import com.invision.notes.models.Note;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface NoteRepository extends CrudRepository<Note, UUID> {
    @Query("select n from Note n where n.status != 'removed'")
    Iterable<Note> findAllValid();
}
