package com.invision.notes.models;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@JsonDeserialize
public class Note {

    @Id
    public UUID id;
    public String type;
    public String title;

    @Column(length=10485760)
    public String content;
    public String status;

    public Note () {

    }

    public Note(String type, String title, String content, String status) {
        this.id = UUID.randomUUID();
        this.type = type;
        this.title = title;
        this.content = content;
        this.status = status;
    }
}
