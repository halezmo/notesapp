package com.invision.notes.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.invision.notes.models.Note;
import com.invision.notes.services.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
public class NoteController {

    @Autowired
    NoteService noteService;

    @ResponseBody
    @GetMapping("/note")
    public ResponseEntity<?> notes() {
        ArrayList<Note> notes = (ArrayList<Note>) noteService.getNotes();
        return buildResponseEntity(notes, HttpStatus.OK);
    }

    @ResponseBody
    @PostMapping("/note")
    public ResponseEntity<Note> add(@RequestBody String payload) throws IOException {
        Note note = noteService.create(payload);
        return buildResponseEntity(note, HttpStatus.CREATED);
    }

    @ResponseBody
    @PutMapping("/note/{id}")
    public ResponseEntity<Note> update(@PathVariable String id, @RequestBody String payload) throws IOException {
        Note note = noteService.update(id, payload);
        return buildResponseEntity(note, HttpStatus.OK);
    }

    @ResponseBody
    @DeleteMapping("/note/{id}")
    public ResponseEntity<Note> delete(@PathVariable String id) throws IOException {
        Note note = noteService.delete(id);
        return buildResponseEntity(note, HttpStatus.OK);
    }

    @ResponseBody
    @DeleteMapping("clear")
    public ResponseEntity<?> clearTrash(@RequestBody String payload) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String[]  ids = mapper.readValue(payload, String[].class);
        List<String> idList = Arrays.asList(ids);
        ArrayList<Note> notes = new ArrayList<>();
        idList.forEach(s -> {
            Note note = noteService.delete(s);
            notes.add(note);
        });
        return buildResponseEntity(notes, HttpStatus.OK);
    }

    private ResponseEntity buildResponseEntity(Object entity, HttpStatus httpStatus) {
        ResponseEntity responseEntity = new ResponseEntity(HttpStatus.NOT_FOUND);
        if (entity != null) {
            responseEntity = new ResponseEntity(entity, httpStatus);
        }
        return responseEntity;
    }
}