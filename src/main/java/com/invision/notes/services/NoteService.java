package com.invision.notes.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.invision.notes.models.Note;
import com.invision.notes.repository.NoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@Service
public class NoteService {

    @Autowired
    NoteRepository noteRepository;

    public Iterable<Note> getNotes() {
        return noteRepository.findAllValid();
    }

    public Note update(String id, String payload) throws IOException {
        Note note = findById(id);
        if (note != null) {
            ObjectMapper mapper = new ObjectMapper();
            Note obj = mapper.readValue(payload, Note.class);
            if (obj != null) {
                if (obj.type != null) {
                    note.type = obj.type;
                }
                if (obj.title != null) {
                    note.title = obj.title;
                }
                if (obj.content != null) {
                    note.content = obj.content;
                }
                if (obj.status != null) {
                    note.status = obj.status;
                }
                noteRepository.save(note);
            }
        }
        return note;
    }

    public Note create(String payload) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Note note= mapper.readValue(payload, Note.class);
        return this.addNote(note.type, note.title, note.content, "active");
    }

    public Note delete(String id) {
        // soft delete set status to removed
        Note note = findById(id);
        note.status = "removed";
        return noteRepository.save(note);
    }

    private Note findById(String id){
        Optional<Note> note = noteRepository.findById(UUID.fromString(id));
        return note.isPresent() ? note.get() : null;
    }

    private Note addNote(String type, String title, String content, String status) {
        return noteRepository.save(new Note(type, title, content, status));
    }
}
