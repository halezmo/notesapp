java 8
spring boot
h2 database

requires maven

package: `mvn package`

produces jar file

jar can be started via: `java -jar ./target/notes-0.0.1-SNAPSHOT.jar`

or

start: `mvn spring-boot:run` 

h2-console: `http://localhost:8080/h2console`

dbName: `jdbc:h2:mem:noteAppDb`